//
//
//  RoadblockData.swift
//  HeyJudeSDK
//
//  Created by Andy Whale on 2019/10/17.
//  Copyright © 2019 TCT Digital. All rights reserved.
//

import Foundation

public class RoadblockData: Decodable, CustomStringConvertible {
      
    
    public var body: String?
    public var body2 : String?
    public var detail: String?
    public var detail2: String?
    public var duration: String?
    public var hasVerifiedEmail: Bool?
    public var hasPaymentMethod: Bool?
    public var paymentMethodRequired: Bool?
    public var subscriptionProposition: SubscriptionProposition?
    public var subscriptionStatus: SubscriptionStatus?
    public var task: Task?
    public var heading: String?
    public var heading2: String?
    public var title: String?
    public var type: String?
    public var buttonLabel: String?
    
    public var topButtonLabel: String?
    public var bottomButtonLabel: String?
    public var postPaidSubscriptionOptionId: Int?
    
    public var sliderMaximumValue: String?
    public var sliderMinimumValue: String?
    public var sliderStepValue: String?
    public var sliderMidPointValue: String?
    public var currency: String?
    
    //survey
    public var message: String?
    public var incentive: String?
    public var token: String?
    public var smIdentifier: String?
    
    public var actions: [RoadblockAction]?
    
    public var subscriptionOptions: [SubscriptionOption]?
    public var highlightedSubscriptionOption: Int?
    
    public var paymentRequestId: Int?
    
    public init () {}
   
    enum CodingKeys : String, CodingKey {
        case heading
        case heading2 = "heading_2"
        case body
        case body2 = "body_2"
        case buttonLabel = "button_label"
        case detail
        case detail2 = "detail_2"
        case duration
        case hasVerifiedEmail = "has_verified_email"
        case hasPaymentMethod = "has_payment_method"
        case paymentMethodRequired = "payment_method_required"
        case subscriptionProposition = "subscription_option"
        case subscriptionStatus = "subscription_status"
        case task
        case title
        case type
        
        case topButtonLabel = "button_1"
        case bottomButtonLabel = "button_2"
        case postPaidSubscriptionOptionId = "post_paid_subscription_option_id"
        
        case message
        case incentive
        case token
        case smIdentifier = "sm_identifier"
        case actions
        case subscriptionOptions = "subscription_options"
        case highlightedSubscriptionOption = "highlighted_subscription_option"
        
        case sliderMaximumValue = "slider_max_value"
        case sliderMinimumValue = "slider_min_value"
        case sliderStepValue = "slider_step_value"
        case sliderMidPointValue = "task_post_paid_midpoint"
        case currency
        case paymentRequestId = "payment_request_id"
    }
   
    public var description: String {
        var description = "\n*******RoadblockData*******\n"
        
        if(self.detail != nil) {
            description += "detail: \(self.detail!)\n"
        }
        
        if(self.heading != nil) {
            description += "heading: \(self.heading!)\n"
        }
        
        if(self.heading2 != nil) {
            description += "heading2: \(self.heading2!)\n"
        }
        
        if(self.detail != nil) {
            description += "detail: \(self.detail!)\n"
        }
        
        if(self.detail2 != nil) {
            description += "detail2: \(self.detail2!)\n"
        }
        
        if(self.body != nil) {
            description += "body: \(self.body!)\n"
        }
        
        if(self.body2 != nil) {
            description += "body2: \(self.body2!)\n"
        }
        
        if(self.buttonLabel != nil){
            description += "buttonLabel: \(self.buttonLabel!)"
        }
        
        if(self.duration != nil) {
            description += "days: \(self.duration!)\n"
        }
        
        if(self.hasVerifiedEmail != nil) {
            description += "hasVerifiedEmail: \(self.hasVerifiedEmail!)\n"
        }
        
        if(self.hasPaymentMethod != nil) {
            description += "hasPaymentMethod: \(self.hasPaymentMethod!)\n"
        }
        
        if(self.paymentMethodRequired != nil) {
            description += "paymentMethodRequired: \(self.paymentMethodRequired!)\n"
        }
        
        if(self.subscriptionProposition != nil) {
            description += "valueProposition: \(self.subscriptionProposition!)\n"
        }
        
        if(self.subscriptionStatus != nil) {
            description += "subscriptionStatus: \(self.subscriptionStatus!)\n"
        }
        
        if(self.task != nil) {
            description += "task: \(self.task!)\n"
        }
        
        if(self.title != nil) {
            description += "title: \(self.title!)\n"
        }
        
        if(self.type != nil) {
            description += "type: \(self.type!)\n"
        }
        
        if (self.message != nil) {
            description += "message: \(self.message!)\n"
        }
        
        if(self.incentive != nil) {
            description += "incentive: \(self.incentive!)\n"
        }
        
        if(self.token != nil) {
            description += "token: \(self.token!)\n"
        }
        
        if(self.smIdentifier != nil) {
            description += "smIdentifier: \(self.smIdentifier!)\n"
        }
        
        if (self.actions != nil) {
            description += "actions: \(self.actions!)\n"
        }
        
        if(self.subscriptionOptions != nil) {
            description += "subscriptionOptions: \(self.subscriptionOptions!)\n"
        }
        
        if(self.highlightedSubscriptionOption != nil) {
            description += "highlightedSubscriptionOption: \(self.highlightedSubscriptionOption!)\n"
        }
        
        if(self.topButtonLabel != nil) {
            description += "topButtonLabel: \(self.topButtonLabel!)\n"
        }
        
        if(self.bottomButtonLabel != nil) {
            description += "bottomButtonLabel: \(self.bottomButtonLabel!)\n"
        }
        
        if(self.postPaidSubscriptionOptionId != nil) {
            description += "postPaidSubscriptionOptionId: \(self.postPaidSubscriptionOptionId!)\n"
        }
        
        if(self.sliderMaximumValue != nil) {
            description += "sliderMaximumValue: \(self.sliderMaximumValue!)\n"
        }
        
        if(self.sliderMinimumValue != nil) {
            description += "sliderMinimumValue: \(self.sliderMinimumValue!)\n"
        }
        
        if(self.sliderStepValue != nil) {
            description += "sliderStepValue: \(self.sliderStepValue!)\n"
        }
        
        if(self.postPaidSubscriptionOptionId != nil) {
            description += "sliderMidPointValue: \(self.sliderMidPointValue!)\n"
        }
        
        if(self.currency != nil) {
            description += "currency: \(self.currency!)\n"
        }
        
        if(self.paymentRequestId != nil) {
            description += "paymentRequestId: \(self.paymentRequestId!)\n"
        }
        
        return description
    }

}
