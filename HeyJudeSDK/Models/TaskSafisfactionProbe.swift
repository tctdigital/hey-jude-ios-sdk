//
//  TaskSafisfactionProbe.swift
//  HeyJudeSDK
//
//  Created by Andy Whale on 2020/11/23.
//

import Foundation
public class TaskSafisfactionProbe: Decodable, CustomStringConvertible {
    
    
    public var id: Int?
    public var response: String?
    public var connotation: String?
    
    enum CodingKeys : String, CodingKey {
        case id
        case response
        case connotation
    }
    
    public var description: String {
        var description = "\n*******TaskSafisfactionProbe*******\n"
        description += "id: \(self.id!)\n"
        description += "response: \(self.response!)\n"
        description += "connotation: \(self.connotation!)\n"
        return description
    }
    
}


/*

 [ { "id": 1, "response": "Communication", "connotation": "positive" }, { "id": 2, "response": "Professional", "connotation": "positive" } ]

 */
