//
//  SubscriptionProposition.swift
//  HeyJudeSDK
//
//  Created by Andy Whale on 2019/10/17.
//  Copyright © 2019 TCT Digital. All rights reserved.
//

import Foundation

public class SubscriptionProposition: Decodable, CustomStringConvertible {
    
    public var amountTime: Int?
    public var amountUsage: Int?
    public var available: Bool?
    public var currency: String?
    public var subscriptionOptionDescription: String?
    public var duration:String?
    public var id: Int?
    public var isTrial: Bool?
    public var name: String?
    public var price: String?
    public var unitTime:String?
    public var type: String?
    
    enum CodingKeys : String, CodingKey {
        case amountTime = "amount_time"
        case amountUsage = "amount_usage"
        case available
        case currency
        case subscriptionOptionDescription = "description"
        case duration
        case id
        case isTrial = "is_trial"
        case name
        case price
        case unitTime = "unit_time"
        case type
    }
    
    public var description: String {
        var description = "\n*******Subscription Proposition*******\n"
        
        if self.amountTime != nil {
            description += "amountTime: \(self.amountTime!)\n"
        }
        
        if self.amountUsage != nil {
            description += "amountTime: \(self.amountUsage!)\n"
        }
        
        if self.available != nil {
            description += "available: \(self.available!)\n"
        }
        
        if self.currency != nil {
            description += "curreny: \(self.currency!)\n"
        }
        
        if self.subscriptionOptionDescription != nil {
            description += "subscriptionOptionDescription: \(self.subscriptionOptionDescription!)\n"
        }
        
        if self.duration != nil {
            description += "duration: \(self.duration!)\n"
        }
        
        if self.id != nil {
            description += "id: \(self.id!)\n"
        }
        
        if self.isTrial != nil {
            description += "isTrial: \(self.isTrial!)\n"
        }
        
        if self.name != nil {
            description += "name: \(self.name!)\n"
        }
        
        if self.price != nil {
            description += "price: \(self.price!)\n"
        }
        
        if self.unitTime != nil {
            description += "unitTime: \(self.unitTime!)\n"
        }
        
        
        if self.type != nil {
            description += "type: \(self.type!)\n"
        }

        return description
    }
    
}

/*
 
 
 
 
 
 
 
 
 
 public var duration: String?
 */

//--------------------

/*
enum CodingKeys : String, CodingKey {
    
}
*/

//--------------------
 
/*
    "id":13,
    "name":"Basic basket, subscription - 20 USD",
    "description":"15 min limit, Monthly subscription, Specific basket of simple tasks, $20 per mt",
    "currency":"USD",
    "price":"20.00",
    "amount_time":1,
    "unit_time":"month",
    "amount_usage":0,
    "available":true,
    "type":"time",
    "is_trial":false,
    "duration":"1 month"
 */
