//
//  SubscriptionStatus.swift
//  Hey Jude SDK
//
//  Created by Byron Tudhope on 2018/07/18.
//  Copyright © 2018 TCT Digital. All rights reserved.
//

public class SubscriptionStatus: Decodable, CustomStringConvertible {


    public var valid: Bool?
    public var displayAutoRenew: Bool?
    public var displayReactivate: Bool?
    public var subscriptionTitle: String?
    public var subscriptionDetail: String?
    public var subscriptionRenewalTitle: String?
    public var subscriptionRenewalDetail: String?
    public var hasVerifiedEmail: Bool?
    public var hasPaymentMethod: Bool?
    public var paymentMethod: String?
    public var subscriptionPrice: String?
    public var subscriptionUnit: String?
    
    public var autoRenew: Bool?
    public var trial: Bool?
    public var renewalDate: String?
    public var expiryText: String?
    public var currentSubscriptionOption: Int?
    public var chosenSubscriptionOption: Int?


    enum CodingKeys : String, CodingKey {
        
        //common across both options
        case autoRenew = "auto_renew"
        
        //Page specific
        case displayAutoRenew = "display_auto_renew"
        case displayReactivate = "display_reactivate"
        case subscriptionTitle = "subscription_title"
        case subscriptionDetail = "subscription_detail"
        case subscriptionRenewalTitle = "subscription_renewal_title"
        case subscriptionRenewalDetail = "subscription_renewal_detail"
        case hasVerifiedEmail = "has_verified_email"
        case hasPaymentMethod = "has_payment_method"
        case paymentMethod = "payment_method"
        case subscriptionPrice = "subscription_price"
        case subscriptionUnit = "subscription_unit"
        
        
        //Hey Jude Specific
        case valid
        case trial = "trial"
        case renewalDate = "renewal_date"
        case expiryText = "expiry_text"
        case currentSubscriptionOption = "current_subscription_option"
        case chosenSubscriptionOption = "chosen_subscription_option"
    }

    public var description: String {
        var description = "\n*******Subscription Status*******\n"
        
        if(self.autoRenew != nil) {
            description += "autoRenew: \(self.autoRenew!)\n"
        }
        
        if(self.hasVerifiedEmail != nil) {
            description += "hasVerifiedEmail: \(self.hasVerifiedEmail!)\n"
        }
        
        if(self.hasPaymentMethod != nil) {
            description += "hasPaymentMethod: \(self.hasPaymentMethod!)\n"
        }
        
        if(self.paymentMethod != nil) {
            description += "paymentMethod: \(self.paymentMethod!)\n"
        }
        
        if(self.subscriptionPrice != nil) {
            description += "subscriptionPrice: \(self.subscriptionPrice!)\n"
        }
        
        if(self.subscriptionUnit != nil) {
            description += "subscriptionUnit: \(self.subscriptionUnit!)\n"
        }
        
        if(self.subscriptionTitle != nil) {
            description += "subscriptionTitle: \(self.subscriptionTitle!)\n"
        }
        
        if(self.subscriptionDetail != nil) {
            description += "subscriptionDetail: \(self.subscriptionDetail!)\n"
        }
        
        if(self.subscriptionRenewalTitle != nil) {
            description += "subscriptionRenewalTitle: \(self.subscriptionRenewalTitle!)\n"
        }
        
        if(self.subscriptionRenewalDetail != nil) {
            description += "subscriptionRenewalDetail: \(self.subscriptionRenewalDetail!)\n"
        }
        
        if(self.displayAutoRenew != nil) {
            description += "displayAutoRenew: \(self.displayAutoRenew!)\n"
        }
        
        if(self.displayReactivate != nil) {
            description += "displayReactivate: \(self.displayReactivate!)\n"
        }
        
        
        if(self.trial != nil) {
            description += "valid: \(self.valid!)\n"
        }
        
        if(self.trial != nil) {
            description += "trial: \(self.trial!)\n"
        }
        
        if(self.renewalDate != nil) {
            description += "renewalDate: \(self.renewalDate!)\n"
        }
        
        if(self.expiryText != nil) {
            description += "expiryText: \(self.expiryText!)\n"
        }
        
        if(self.currentSubscriptionOption != nil) {
            description += "currentSubscriptionOption: \(self.currentSubscriptionOption!)\n"
        }
        
        if(self.chosenSubscriptionOption != nil) {
            description += "chosenSubscriptionOption: \(self.chosenSubscriptionOption!)\n"
        }
        
        
        return description
    }
}

//{
//  "data": {
//    "subscription_status": {
//      "display_auto_renew": false,
//      "display_reactivate": true,
//      "auto_renew": false,
//      "subscription_detail": "Your trial has come to an end.",
//      "subscription_renewal_detail": "Sign up now for $30.00 per month."
//    }
//  },
//  "errors": [],
//  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9oZXJtZXMudGN0ZGlnaXRhbC54eXpcL2FwaVwvdjFcL2F1dGhcL3NpZ24taW4iLCJpYXQiOjE1NzI0MzE3NzAsImV4cCI6MTU3MjQzNTM3MCwibmJmIjoxNTcyNDMxNzcwLCJqdGkiOiJNTWtoRG9FNzloc0czZ0VmIiwic3ViIjoyMywicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSIsImF1dGhvcml6ZWQiOjEsInVzZXJfaWQiOjIzfQ.oMP1y5LFmIHUn09LAfMSy2OdNbqhbaWlCPtqCH9Svvw",
//  "message": "Success",
//  "success": true
//}
