//
//  Information.swift
//  FirebaseCore
//
//  Created by Byron Tudhope on 2019/10/25.
//

import Foundation

public class Information: Decodable, CustomStringConvertible {
    
    public var accountInformation: [InformationItem]?
    public var deviceInformation: [InformationItem]?
    public var systemInformation: [InformationItem]?
    public var otherInformation: [InformationItem]?

    enum CodingKeys : String, CodingKey {
        case accountInformation = "account_information"
        case deviceInformation = "device_information"
        case systemInformation = "system_information"
        case otherInformation = "other_information"
    }

    public var description: String {
        var description = "\n*******Information*******\n"

        if (self.accountInformation != nil && self.accountInformation?.count ?? 0 > 0) {
            description += "accountInformation: \(self.accountInformation!.description)\n"
        }

        if (self.deviceInformation != nil && self.deviceInformation?.count ?? 0 > 0) {
            description += "deviceInformation: \(self.deviceInformation!.description)\n"
        }

        if (self.systemInformation != nil && self.systemInformation?.count ?? 0 > 0) {
            description += "systemInformation: \(self.systemInformation!.description)\n"
        }

        if (self.otherInformation != nil && self.otherInformation?.count ?? 0 > 0) {
            description += "otherInformation: \(self.otherInformation!.description)\n"
        }


        return description
    }

}
