//
//  PaymentProvider.swift
//  HeyJudeSDK
//
//  Created by Wayne Eldridge on 2019/06/24.
//  Copyright © 2019 TCT Digital. All rights reserved.
//

import Foundation

public enum PaymentProviderIdentifier: String {
    case peach
    case stripe
    case getmore
}

public class PaymentProviderCredentials: Decodable, CustomStringConvertible {
    public var token: String?
//    public var userId: String?
//    public var password: String?
    public var entityId: String?
    
    enum CodingKeys: String, CodingKey {
        case token
//        case userId = "user_id"
//        case password
        case entityId = "entity_id"
    }
    
    public var description: String {
        var description = "\n*******Payment Credentials *******\n"
        
        if self.token != nil {
            description += "token: \(self.token!)"
        }
        
//        if self.userId != nil {
//            description += "userId: \(self.userId!)\n"
//        }
//
//        if self.password != nil {
//            description += "password: \(self.password!)\n"
//        }
        
        if self.entityId != nil {
            description += "entityId: \(self.entityId!)\n"
        }
        
        return description
    }
}

public class PaymentProvider: Decodable, CustomStringConvertible {
    
    public var identifier: String?
    public var provider: String?
    public var credentials: PaymentProviderCredentials?
    //public var params: CLASS
    
    enum CodingKeys: String, CodingKey {
        case identifier
        case provider
        case credentials = "required_parameters"
    }
    
    public var description: String {
        var description = "\n*******Payment Provider*******\n"
        
        if self.identifier != nil {
            description += "identifier: \(self.identifier!)\n"
        }
        
        if self.provider != nil {
            description += "provider: \(self.provider!)\n"
        }
        
        if self.credentials != nil {
            description += "credentials: \(self.credentials!)"
        }
        
        return description
    }
    
}
