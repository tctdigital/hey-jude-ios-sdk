//
//  Suggestion.swift
//  HeyJudeSDK
//
//  Created by Andy Whale on 2020/05/06.
//  Copyright © 2020 TCT Digital. All rights reserved.
//

import Foundation

public class Suggestion: Decodable, CustomStringConvertible {
    public var id: Int?
    public var key: String?
    public var name: String?
    public var template: String?
    public var icon: String?
    public var isFrequent: Bool?
    
    public init () {}
    
    enum CodingKeys : String, CodingKey {
        case id
        case key
        case name
        case template
        case icon
        case isFrequent = "is_frequent"
    }
    
    public var description: String {
        var description = "\n*******Task Suggestion*******\n"
        description += "id: \(self.id!)\n"
        description += "key: \(self.key!)\n"
        description += "name: \(self.name!)\n"
        description += "template: \(self.template!)"
        description += "icon: \(self.icon!)\n"
        description += "isFrequent: \(self.isFrequent!)\n"
        return description
    }
}

