//
//  Data.swift
//  Hey Jude SDK
//
//  Created by Byron Tudhope on 2018/07/15.
//  Copyright © 2018 TCT Digital. All rights reserved.
//

public class Data: NSObject, Decodable {

    public var user: User?
    public var reasons: [Reason]?
    public var roadblock: Roadblock?
    public var roadblocks: [Roadblock]?
    public var ideas: [Idea]?
    public var tasks: [Task]?
    public var suggestions: [Suggestion]?
    public var paymentProviders:[PaymentProvider]?
    public var paymentMethods: [PaymentMethod]?
    public var payments: [Payment]?
    public var paymentRequest: PaymentRequest?
    public var task: Task?
    public var subscriptionOptions: [SubscriptionOption]?
    public var subscriptionStatus: SubscriptionStatus?
    public var attachment: Attachment?
    public var featuredImage: FeaturedImage?
    public var countries: [Country]?
    public var contactCenters: [ContactCenter]?
    public var taskSafisfactionProbes: [TaskSafisfactionProbe]?
    public var taskRating: Int?
    public var signupToken: String?
    public var waitlistRequired: Bool?
    public var pricePointNotification: String?
    public var tutorialCopy1: String?
    public var tutorialCopy2: String?
    public var tutorialCopy3: String?
    public var referralCopy: String?
    public var showInviteScreen: Bool?
    public var invalidInviteCode: Bool?
    public var inviteCodeCopy: String?
    public var information: Information?
    public var hasVerifiedEmail: Bool?
    public var message: Message?
    public var validRegion: Bool?
    public var regionMessage: String?
    public var createOnboardingTask: Bool?
    public var createOnboardingTaskTitle: String?
    public var notice: String?

    enum CodingKeys : String, CodingKey {
        case user
        case reasons
        case roadblock
        case roadblocks
        case ideas
        case tasks
        case suggestions
        case paymentProviders = "payment_providers"
        case paymentMethods = "payment_methods"
        case payments
        case paymentRequest = "payment_request"
        case task
        case subscriptionOptions = "subscription_options"
        case subscriptionStatus = "subscription_status"
        case attachment
        case featuredImage = "featured_image"
        case countries
        case contactCenters = "contact_centers"
        case taskSafisfactionProbes = "task_satisfaction_probes"
        case taskRating = "task_rating"
        case signupToken = "signup_token"
        case waitlistRequired = "waitlist_required"
        case pricePointNotification = "price_point_text"
        case tutorialCopy1 = "tutorial_copy_1"
        case tutorialCopy2 = "tutorial_copy_2"
        case tutorialCopy3 = "tutorial_copy_3"
        case referralCopy = "referral_text"
        case information
        case hasVerifiedEmail = "has_verified_email"
        case message
        case validRegion = "valid_geographical_region"
        case regionMessage = "invalid_geographical_region_message"
        case showInviteScreen = "show_invite_screen"
        case invalidInviteCode = "invalid_invite_code"
        case inviteCodeCopy = "invite_code_copy"
        case createOnboardingTask = "create_onboarding_task"
        case createOnboardingTaskTitle = "create_onboarding_task_title"
        case notice = "notice"
    }

}
