//
//  InformationItem.swift
//  FirebaseCore
//
//  Created by Byron Tudhope on 2019/10/25.
//

import Foundation

public class InformationItem: Decodable, CustomStringConvertible {
    
    public var key: String
    public var name: String
    public var value: String

    enum CodingKeys : String, CodingKey {
        case key
        case name
        case value
    }

    public var description: String {
        var description = "\n*******InformationItem*******\n"

        description += "key: \(self.key)\n"
        description += "name: \(self.name)\n"
        description += "value: \(self.value)\n"

        return description
    }

}
