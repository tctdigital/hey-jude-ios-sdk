//
//  RoadblockAction.swift
//  HeyJudeSDK
//
//  Created by Andy Whale on 2020/05/15.
//  Copyright © 2020 TCT Digital. All rights reserved.
//

import Foundation
public class RoadblockAction: Decodable, CustomStringConvertible {
    public var type: String?
    public var key: String?
    public var value: String?
    
    public init () {}
    
    enum CodingKeys : String, CodingKey {
        case type
        case key
        case value
    }
    
    public var description: String {
        var description = "\n*******RoadblockActions*******\n"
        description += "type: \(self.type!)\n"
        description += "key: \(self.key!)\n"
        if(self.value != nil) {
            description += "value: \(self.value!)\n"
        }else {
            description += "value: nil\n"
        }
        return description
    }
}

