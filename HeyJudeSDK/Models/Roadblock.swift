//
//  Roadblock.swift
//  Hey Jude SDK
//
//  Created by Byron Tudhope on 2018/07/18.
//  Copyright © 2018 TCT Digital. All rights reserved.
//

public class Roadblock: Decodable, CustomStringConvertible {
    
    public var id: Int?
    public var name: String?
    public var identifier: String?
    public var type: String?
    public var action: String?
    public var priority: Int?
    public var display: String?
    public var error: String?
    public var data: RoadblockData?

    public var description: String {
        var description = "\n*******Roadblock*******\n"
        description += "id: \(self.id!)\n"
        description += "name: \(self.name!)\n"
        description += "identifier: \(self.identifier!)\n"
        description += "type: \(self.type!)\n"
        description += "action: \(self.action!)\n"
        description += "priority: \(self.priority!)\n"
        description += "display: \(self.display!)\n"
        description += "***data: \(self.data!)***\n"
        description += "***error: \(self.error ?? "")***\n"
        
        return description
    }

    enum CodingKeys : String, CodingKey {
        case id
        case name
        case identifier
        case type
        case action
        case priority
        case display
        case error
        case data
//        case paymentMethodRequired = "payment_method_required"
    }
}
