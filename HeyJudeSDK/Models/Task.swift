//
//  Task.swift
//  Hey Jude SDK
//
//  Created by Byron Tudhope on 2018/07/16.
//  Copyright © 2018 TCT Digital. All rights reserved.
//

public class Task: Decodable, CustomStringConvertible {

    public var id: Int?
    public var title: String?
    public var createdAt: String?
    public var status: String?
    public var open: Bool?
    public var unread: Bool?
    public var messages: [Message]?
    public var unixTimestamp: String?
    public var triggerSatisfactionProbe: Bool?

    public init () {}

    enum CodingKeys : String, CodingKey {
        case id
        case title
        case createdAt = "created_at"
        case status
        case open
        case unread
        case messages
        case unixTimestamp = "timestamp"
        case triggerSatisfactionProbe = "trigger_satisfaction_probe"
    }

    public var description: String {
        var description = "\n*******Task*******\n"
        description += "id: \(self.id!)\n"
        description += "title: \(self.title!)\n"
        description += "createdAt: \(self.createdAt!)\n"
        description += "unixTimestamp: \(self.unixTimestamp!)\n"
        description += "status: \(self.status!)\n"
        description += "open: \(self.open!)\n"
        description += "unread: \(self.unread!)\n"
        
        if self.triggerSatisfactionProbe != nil {
            description += "triggerSatisfactionProbe: \(self.triggerSatisfactionProbe!)\n"
        }
        
        if self.messages != nil {
            description += "messages: \n"
            for message in self.messages! {
                description += "\(message.description)\n"
            }
        }
        return description
    }
}
