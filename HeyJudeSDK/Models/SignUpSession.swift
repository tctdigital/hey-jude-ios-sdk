//
//  SignUpSession.swift
//  Hey Jude SDK
//
//

public class SignUpSession: Decodable, CustomStringConvertible {

    public var waitlistRequired: Bool?
    public var signupToken: String?
    public var tutorialCopy1: String?
    public var tutorialCopy2: String?
    public var tutorialCopy3: String?
    public var validRegion: Bool?
    public var regionMessage: String?
    public var showInviteScreen: Bool?
    public var invalidInviteCode: Bool?
    public var inviteCodeCopy: String?
    public var createOnboardingTask: Bool?
    public var createOnboardingTaskTitle: String?

    public var description: String {
        var description = "\n*******SignupSession*******\n"
        description += "waitlistRequired: \(self.waitlistRequired!)\n"
        description += "signupToken: \(self.signupToken!)\n"
        description += "tutorialCopy1: \(self.tutorialCopy1!)\n"
        description += "tutorialCopy2: \(self.tutorialCopy2!)\n"
        description += "tutorialCopy3: \(self.tutorialCopy3!)\n"
        description += "validRegion: \(self.validRegion!)\n"
        description += "regionMessage: \(self.regionMessage!)"
        description += "showInviteScreen: \(self.showInviteScreen!)\n"
        description += "invalidInviteCode: \(self.invalidInviteCode!)\n"
        description += "inviteCodeCopy: \(self.inviteCodeCopy!)\n"
        description += "createOnboardingTask: \(self.createOnboardingTask!)\n"
        description += "createOnboardingTaskTitle: \(self.createOnboardingTaskTitle!)\n"
        return description
    }

    enum CodingKeys : String, CodingKey {
        case waitlistRequired = "waitlist_required"
        case signupToken = "singup_token"
        case tutorialCopy1 = "tutorial_copy_1"
        case tutorialCopy2 = "tutorial_copy_2"
        case tutorialCopy3 = "tutorial_copy_3"
        case validRegion = "valid_geographical_region"
        case regionMessage = "invalid_geographical_region_message"
        case showInviteScreen = "show_invite_screen"
        case invalidInviteCode = "invalid_invite_code"
        case inviteCodeCopy = "invite_code_copy"
        case createOnboardingTask = "create_onboarding_task"
        case createOnboardingTaskTitle = "create_onboarding_task_title"
    }
}
