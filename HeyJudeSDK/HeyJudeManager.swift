//
//  HeyJudeManager.swift
//  Hey Jude SDK
//
//  Created by Byron Tudhope on 2018/07/15.
//  Copyright © 2018 TCT Digital. All rights reserved.
//

import MobileCoreServices
import CoreLocation
import CoreTelephony
import SocketIO

public enum HeyJudeEnvironment {
    case LIVE
    case STAGING
    case DEVELOPMENT
    
    var intValue: Int {
        switch self {
        case .LIVE: return 0
        case .STAGING: return 1
        case .DEVELOPMENT: return 2
        }
    }
}

open class HeyJudeManager: NSObject, CLLocationManagerDelegate {
    
    private let locationManager = CLLocationManager()
    private var currentLocation: CLLocation!
    private var environment: Int
    private var program: String
    private var apiKey: String
    private var token: String = ""
    private var userId: Int = 0
    private var socket: SocketIOClient?
    private var manager: SocketManager?
    
//    private var paymentProviders: [PaymentProvider]?
    
    public init(environment: HeyJudeEnvironment, program: String, apiKey: String) {
        
        self.environment = environment.intValue;
        self.program = program;
        self.apiKey = apiKey;
        
        super.init()
        
        if #available(iOS 9.0, *) {
            locationManager.delegate = self
            locationManager.requestLocation()
        }
        
        self.initSocket()
        
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            //print("Found user's location: \(location)")
            self.currentLocation = location
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    //MARK : requestLocation is async, can't neatly add into existing methods : Detecting change of permission state and capturing the value
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if(status == .authorizedWhenInUse){
            locationManager.requestLocation()
        }
    }
    
    // MARK: SocketIO
    
    private func initSocket() {
        self.manager = SocketManager(socketURL: URL(string: self.socketHost())!,
                                     config: [.log(false),
                                              .secure(true),
                                              .forceWebsockets(false),
                                              .connectParams(["user_id": self.userId, "token": self.token]),
                                              .path("/")
                                        
            ])
        
        if self.manager != nil {
            self.manager?.reconnects = true
            self.manager?.forceNew = true
            self.socket = self.manager?.socket(forNamespace: "/")
        }
        
        self.addHandlers()
    }
    
    private func connectSocket() {
        self.socket!.connect()
    }
    
    private func configureSocket() {
        
        if self.manager == nil {
            self.initSocket()
        }
        
        if self.manager != nil {
            self.manager?.setConfigs([.log(false),
                                      .secure(true),
                                      .forceWebsockets(false),
                                      .connectParams(["user_id": self.userId, "token": self.token]),
                                      .path("/")
                ])
            
            self.manager?.reconnect()
        }
    }
    
    private func disconnectSocket() {
        if self.socket != nil {
            self.socket!.disconnect()
        }
    }
    
    private func clearSocket() {
        self.socket = nil
        self.manager = nil
    }
    
    private func addHandlers() {
        
        self.socket?.onAny { (data) in
            if data.event == "reconnectAttempt" {
                if self.manager != nil {
                    self.manager?.disconnect()
                    self.socket?.connect()
                }
            }
        }
    }
    
    // MARK: - Bind to Chat Status
    open func BindToChatStatus(completion: @escaping (_ status: String) -> ()) {
        
        if self.socket != nil {
            self.socket?.on(clientEvent: .connect) {data, ack in
                completion("connected")
            }
            
            self.socket?.on(clientEvent: .disconnect) {data, ack in
                completion("disconnected")
            }
            
            self.socket?.on(clientEvent: .reconnectAttempt) {data, ack in
                completion("reconnecting")
            }
            
            self.socket?.on(clientEvent: .reconnect) {data, ack in
                completion("reconnecting")
            }
        }
        
    }
    
    // MARK: - Bind to Chat
    open func BindToChat(completion: @escaping (_ message: Message) -> ()) {
        
        if self.socket != nil {
            self.socket?.on("chat:" + String(self.userId) + ":messages") {data, ack in
                
                print("Message Received!")
                let messageJson = data[0] as! String
                
                let messageData = messageJson.data(using: .utf8)
                
                guard let message = try? JSONDecoder().decode(Message.self, from: messageData!) else {
                    print("Error: Couldn't decode data into Response")
                    return
                }
                completion(message)
            }
        }
    }
    
    private func userAuthenticated(user: User, pushnotificationToken: String) {
        self.userId = user.id!
        self.configureSocket()
        self.connectSocket()
//        AnalyticsControl.sendUserAnalytics()
        self.Analytics(pushToken: pushnotificationToken) { (success, error) in }
        self.ResetBadgeCount() { (success, error) in }
    }
    
    private func userSignedOut() {
        self.userId = 0
        self.token = ""
        self.disconnectSocket()
        self.clearSocket()
    }
    
    // MARK: Auth
    
    // MARK: - Pause Session
    open func PauseSession() -> String {
        let token = self.token
//        self.userSignedOut()
        self.disconnectSocket()
        return token
    }
    
    private var appVersion: String = {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        } else {
            return "3.0.1"
        }
    }()
    
    // MARK: - Resume Session
    open func ResumeSession(sessionToken: String, pushNotificationToken: String, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()){
        
        let jwtClaims = self.decodeToken(jwtToken: sessionToken)
        if let userId = jwtClaims["sub"] as? Int {
            self.userId = userId
            self.token = sessionToken
            self.Profile() { (success, user, error) in
                if (success) {
                    if let user = user {
                        self.userAuthenticated(user: user,
                                               pushnotificationToken: pushNotificationToken)
                    }
                }
                completion(success, error)
            }
            return
        }
        
        let error = HeyJudeError(httpResponseCode: 401, apiErrors: ["Invalid Token"], requestError: nil, response: nil)
        
        completion(false, error)
        
    }
    
    // MARK: - Sign In for White Labels
    /// - Parameter params: must contain 'username' and 'password' as params
    open func SignInWhiteLabel(params: Dictionary<String, AnyObject>, pushToken: String?, completion: @escaping (_ success: Bool, _ object: User?, _ error: HeyJudeError?) -> ()) {
        let pushToken = pushToken ?? ""
        
        var signInParams = params
        signInParams["program"] = self.program as AnyObject
        signInParams["platform"] =  "ios"  as AnyObject
        signInParams["device_token"] = pushToken  as AnyObject

        post(request: createPostRequest(path: "auth/sign-in", params: signInParams)) { (success, data, error) in
            if let user = data?.user {
                self.userAuthenticated(user: user, pushnotificationToken: pushToken)
                completion(success, user, error)
                
            } else {
                completion(false, nil, error)
            }
        }
    }
    
    open func SignInToken(uniqueId: String, promoCode: String?, firstName: String, lastName:String?, email: String, mobile:String, token: String, pushToken: String?, completion: @escaping (_ success: Bool, _ object: User?, _ error: HeyJudeError?) -> ()) {
        let params: [String: Any] = [
            "profile_id": uniqueId, // the user they want to add / sign in
            "promo_code": promoCode ?? "",
            "first_name": firstName,
            "last_name": lastName ?? "",
            "email": email,
            "mobile": mobile,
            "jwt_token": token,
            //claims must be added
            //"uid" same value as "profile_id" // security check
            "program": self.program,
            "platform": "ios",
            "device_token": pushToken ?? ""
        ]
        
        post(request: createPostRequest(path: "auth/sign-in", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if let user = data?.user {
                self.userAuthenticated(user: user, pushnotificationToken: pushToken ?? "")
                completion(success, user, error)
            } else {
                completion(false, nil, error)
            }
        }
    }
    
    // MARK: - Sign In
    open func SignIn(username: String, password: String, pushToken: String?, completion: @escaping (_ success: Bool, _ object: User?, _ error: HeyJudeError?) -> ()) {
        var params = ["program": self.program]
        let pushToken = pushToken ?? ""
        switch self.program {
        case "nedbank":
            params = ["profile_id": username, "jwt_token": password, "program": self.program, "platform": "ios", "device_token": pushToken]
        default:
            params = ["email": username, "password": password, "program": self.program, "platform": "ios", "device_token": pushToken]
            break
        }
        
        post(request: createPostRequest(path: "auth/sign-in", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if let user = data?.user {
                self.userAuthenticated(user: user, pushnotificationToken: pushToken)
                completion(success, user, error)
            } else {
                completion(false, nil, error)
            }
        }
    }
    
    // MARK: - OAUTH2_SignIn
    open func SignInWithOAuth2(params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ object: User?, _ error: HeyJudeError?) -> ()) {
        var user = params
        user["program"] = self.program as AnyObject
        user["platform"] = "ios" as AnyObject
        
        post(request: createPostRequest(path: "auth/sign-in", params: user as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if let user = data?.user {
                self.userAuthenticated(user: user, pushnotificationToken: "")
                completion(success, user, error)
            } else {
                completion(false, nil, error)
            }
        }
    }
    
    // MARK: - Get Referal Text for PP & VP
    open func ReferralNotification(completion: @escaping (_ success: Bool, _ object: String?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "users/referral-notification")) { (success, data, error) in
            if let referralNotification = data?.referralCopy {
                completion(success, referralNotification, error)
            } else {
                completion(false, nil, error)
            }
        }
    }
    
    // MARK: - Start Sign Up Session
    open func StartSignUpSession(params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ signupSession: SignUpSession?, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "auth/signup-session-start", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            
//            guard let waitlistRequired = data?.waitlistRequired else {
//                completion(false, nil, error)
//                return //fail completion
//            }
            
            guard let signupToken = data?.signupToken else {
                completion(false, nil, error)
                return// fail completion
            }
            
            let signupTutorial1 = data?.tutorialCopy1 ?? ""
            let signupTutorial2 = data?.tutorialCopy2 ?? ""
            let signupTutorial3 = data?.tutorialCopy3 ?? ""
            let validRegion = data?.validRegion ?? true
            let regionMessage = data?.regionMessage ?? ""
            let showInviteScreen = data?.showInviteScreen ?? true
            let invalidInviteCode = data?.invalidInviteCode ?? true
            let inviteCodeCopy = data?.inviteCodeCopy ?? ""
            let createOnboardingTask = data?.createOnboardingTask ?? false
            let createOnboardingTaskTitle = data?.createOnboardingTaskTitle ?? ""
            
            let signupSession = SignUpSession()

            signupSession.waitlistRequired = false
            signupSession.signupToken = signupToken
            signupSession.tutorialCopy1 = signupTutorial1
            signupSession.tutorialCopy2 = signupTutorial2
            signupSession.tutorialCopy3 = signupTutorial3
            signupSession.validRegion = validRegion
            signupSession.regionMessage = regionMessage
            signupSession.showInviteScreen = showInviteScreen
            signupSession.invalidInviteCode = invalidInviteCode
            signupSession.inviteCodeCopy = inviteCodeCopy
            signupSession.createOnboardingTask = createOnboardingTask
            signupSession.createOnboardingTaskTitle = createOnboardingTaskTitle

            completion(success, signupSession, error)
        }
    }
    
    //MARK: - Apply Promo / exchange for new SessionToken
    open func applyPromoCode(params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ signupSession: SignUpSession?, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "auth/verify-invite-code", params: params)) { (success, data, error) in
            
            guard let signupToken = data?.signupToken else {
                completion(false, nil, error)
                return// fail completion
            }
           
           let signupTutorial1 = data?.tutorialCopy1 ?? ""
           let signupTutorial2 = data?.tutorialCopy2 ?? ""
           let signupTutorial3 = data?.tutorialCopy3 ?? ""
           let validRegion = data?.validRegion ?? true
           let regionMessage = data?.regionMessage ?? ""
           let showInviteScreen = data?.showInviteScreen ?? true
           let invalidInviteCode = data?.invalidInviteCode ?? true
           let inviteCodeCopy = data?.inviteCodeCopy ?? ""
           let createOnboardingTask = data?.createOnboardingTask ?? false
            let createOnboardingTaskTitle = data?.createOnboardingTaskTitle ?? ""
           
           let signupSession = SignUpSession()

           signupSession.waitlistRequired =  false
           signupSession.signupToken = signupToken
           signupSession.tutorialCopy1 = signupTutorial1
           signupSession.tutorialCopy2 = signupTutorial2
           signupSession.tutorialCopy3 = signupTutorial3
           signupSession.validRegion = validRegion
           signupSession.regionMessage = regionMessage
           signupSession.showInviteScreen = showInviteScreen
           signupSession.invalidInviteCode = invalidInviteCode
           signupSession.inviteCodeCopy = inviteCodeCopy
           signupSession.createOnboardingTask = createOnboardingTask
           signupSession.createOnboardingTaskTitle = createOnboardingTaskTitle

           completion(success, signupSession, error)
        }
    }
    
    // MARK: - Wait list sign up
    open func WaitlistSignUp(signup_token: String, email: String, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        
        let params = ["signup_token": signup_token, "email": email]
        post(request: createPostRequest(path: "auth/waitlist-sign-up", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            completion(success, error)
        }
    }
    
    //MARK: - User Email Verification
    open func userEmailVerified(completion: @escaping(_ success: Bool, _ hasVerifiedEmail:Bool, _ error:HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "users/email-verified")) { (success, data, error) in
            if let hasVerifiedEmail = data?.hasVerifiedEmail {
                completion(success, hasVerifiedEmail, error)
            } else {
                completion(false, false, error)
            }
        }
    }
    
    
    // MARK: - Verify Phone
    open func VerifyPhone(mobile: String, type: String, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        let params = ["mobile": mobile, "type": type, "program": self.program]
        post(request: createPostRequest(path: "auth/verify", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            completion(success, error)
        }
    }
    
    // MARK: - Sign Up
    open func SignUp(params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ object: User?, _ error: HeyJudeError?) -> ()) {
        var user = params
        user["program"] = self.program as AnyObject
        user["platform"] = "ios" as AnyObject
        user["country_code"]
//        print(params)
        
        post(request: createPostRequest(path: "auth/sign-up", params: user as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if let user = data?.user {
                self.userAuthenticated(user: user, pushnotificationToken: "")
                completion(success, user, error)
                
            } else {
                completion(false, nil, error)
            }
        }
    }
    
    // MARK: - Forgot Password
    open func ForgotPassword(params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        var forgotParams = params
        forgotParams["program"] =  self.program as AnyObject
        
        post(request: createPostRequest(path: "auth/forgot", params: forgotParams)) { (success, data, error) in
            completion(success, error)
        }
    }
    
    // MARK: - Reset Password
    open func ResetPassword(params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ object: User?, _ error: HeyJudeError?) -> ()) {
        var resetParams = params
        resetParams["program"] = self.program as AnyObject
        post(request: createPostRequest(path: "auth/reset", params: resetParams as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if let user = data?.user {
                completion(success, user, error)
            } else {
                completion(false, nil, error)
            }
        }
    }
    
    // MARK: - Report a Problem
    open func ReportAProblem(params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        let screenSize = UIScreen.main.nativeBounds
        let screenWidth = Int(screenSize.width)
        let screenHeight = Int(screenSize.height)
        
        // Get carrier name
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        let carrierName = carrier?.carrierName ?? ""
        
        var locationEnabled = false
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
           locationEnabled = true
           break
        case .authorizedAlways:
           locationEnabled = true
           break
        default:
           locationEnabled = false
           break
        }
       
        let analyticParams = [
            "app_version": self.appVersion,//"3.0.0",
            "app_push_enabled": UIApplication.shared.isRegisteredForRemoteNotifications,
            "app_location_enabled": locationEnabled,
            "device_os": "iOS",
            "device_os_version": UIDevice.current.systemVersion,
            "device_carrier": carrierName,
            "device_screen_size": "\(screenWidth)x\(screenHeight)",
            "program": self.program
        ] as Dictionary<String, AnyObject>
        
        let joinedParams = analyticParams.merging(params) { (_, new) in new }
        
        post(request: createPostRequest(path: "report-a-problem", params: joinedParams)) { (success, _, error) in
                completion(success, error)
        }
    }
    
    // MARK: - Refresh
    open func Refresh(completion: @escaping (_ success: Bool,_ token: String, _ error: HeyJudeError?) -> ()) {
        get(request: createPostRequest(path: "auth/refresh")) { (success, data, error) in
//            print("error :", error.debugDescription)
//            print("token : ", self.token)
            let token = self.token
            completion(success, token, error)
        }
    }
    
    // MARK: - Sign Out
    open func SignOut(completion: @escaping (_ success: Bool) -> ()) {
        get(request: createPostRequest(path: "auth/sign-out")) { (success, data, error) in
            self.userSignedOut()
            completion(success)
        }
    }
    
    // MARK: - Verify Phone
    open func VerifyPhone(mobile: String, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        let params = ["mobile": mobile, "program": self.program]
        post(request: createPostRequest(path: "auth/verify", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            completion(success, error)
        }
    }
    
    // MARK: - Forgot
    open func Forgot(mobile: String, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        let params = ["mobile": mobile, "program": self.program]
        post(request: createPostRequest(path: "auth/forgot", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            completion(success, error)
        }
    }
    
    // MARK: - Reset
    open func Reset(mobile: String, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        let params = ["mobile": mobile, "program": self.program]
        post(request: createPostRequest(path: "auth/reset", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            completion(success, error)
        }
    }
    
    // MARK: User
    // MARK: Profile
    open func Profile(completion: @escaping (_ success: Bool, _ object: User?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "users/profile")) { (success, data, error) in
            if let user = data?.user {
                completion(success, user, error)
            } else {
                completion(false, nil, error)
            }
        }
    }
    // MARK: Survey
    open func Survey(completion: @escaping (_ success: Bool, _ object: Roadblock?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "users/survey")) { (success, data, error) in
            if let roadblock = data?.roadblock {
                completion(success, roadblock, error)
            } else {
                completion(false, nil, error)
            }
        }
    }
    
    // MARK: Price Point Notification
    open func PricePointNotification(completion: @escaping (_ success: Bool, _ object: String, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "users/price-point-notification")) { (success, data, error) in
            if let pricePointNotification = data?.pricePointNotification {
                completion(success, pricePointNotification, error)
            } else {
                completion(false, "", error)
            }
        }
    }
    
    // MARK: User Information
    open func UserInformation(completion: @escaping (_ success: Bool, _ object: Information?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "users/data")) { (success, data, error) in
            if let information = data?.information {
                completion(success, information, error)
            } else {
                completion(false, nil, error)
            }
        }
    }
    
    open func ExportAllTasks(completion: @escaping (_ success: Bool, _ error: HeyJudeError?) ->()) {
        get(request: createGetRequest(path: "users/download-all-tasks-info")) { (success, data, error) in
                completion(success, error)
        }
    }

    open func FetchEndNoticeText(completion: @escaping (_ success: Bool, _ endNoticeText: String?, _ error: HeyJudeError?) ->()) {
        get(request: createGetRequest(path: "end-notice")) { (success, data, error) in
            if let endNoticeText = data?.notice {
                completion(success, endNoticeText, error)
            } else {
                completion(false, nil, error)
            }
        }
    }
    
    // MARK: Delete User Information Item
    open func UserDeleteInformationItem(params:[String: [AnyObject]], completion: @escaping(_ success: Bool, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "users/data", params: params)) { (success, data, error) in
            completion(success,error)
        }
    }
    
    // MARK: User Clear Information
    open func UserClearInformation(completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        post(request: createGetRequest(path: "users/clear-data")) { (success, data, error) in
            completion(success, error)
        }
    }
    
    // MARK: Delete User Account
    open func DeleteUserAccount(completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        post(request: createGetRequest(path: "users/delete-account")) { (success, data, error) in
            completion(success, error)
        }
    }
    
    // MARK: Update Profile
    open func UpdateProfile(params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ object: User?, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "users/profile", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            // If a profile photo is attached. We upload that attachment.
            if let filePath = params["profile_image"] as? String {
                self.post(request: self.createMultiPartRequestForProfilePicture(path: "users/profile", filePath: filePath), completion: { (success, data, error) in
                    if let user = data?.user {
                        completion(success, user, error)
                    } else {
                        completion(false, nil, error)
                    }
                })
            } else {
                if let user = data?.user {
                    completion(success, user, error)
                } else {
                    completion(false, nil, error)
                }
            }
        }
    }
    
    // MARK: Get Roadblocks
    open func Roadblocks(completion: @escaping (_ success: Bool, _ object: [Roadblock]?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "roadblocks/index")) { (success, data, error) in
            if let roadblocks = data?.roadblocks {
                completion(success, roadblocks, error)
            } else {
                completion(false, [], error)
            }
        }
    }
    
    // MARK: Skip Roadblock
    open func SkipRoadblock(id: Int, completion: @escaping (_ success: Bool, _ object: Any?, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "roadblocks/" + String(id) + "/skip")) { (success, data, error) in
            if (success) {
                if let task = data?.task {
                completion(success, task, error)
                return
            }
            if let roadblock = data?.roadblock {
                completion(success, roadblock, error)
                return
            }
                
            completion(success, nil, error)
                
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: Update RoadBlock
    open func ActionRoadblock(id: Int, action: String, params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ object: Any?, _ error: HeyJudeError?) -> ()) {
            var allParams = params
            allParams["action"] = action as AnyObject
        
             post(request: createPostRequest(path: "roadblocks/" + String(id) + "/action", params: allParams)) { (success, data, error) in
                if (success) {
                    
                    if let task = data?.task {
                        completion(success, task, error)
                        return
                    }
               
                    if let roadblock = data?.roadblock {
                        completion(success, roadblock, error)
                        return
                    }

                    completion(success, nil, error)
                } else {
                    completion(success, nil, error)
                }
            }
    }
    
    // MARK: Action Roadblock
    open func ActionRoadblock(id: Int, action: String, completion: @escaping (_ success: Bool, _ object: Any?, _ error: HeyJudeError?) -> ()) {
        let params = ["action": action]
        post(request: createPostRequest(path: "roadblocks/" + String(id) + "/action", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                if let task = data?.task {
                completion(success, task, error)
                return
            }
            if let roadblock = data?.roadblock {
                completion(success, roadblock, error)
                return
            }
                
            completion(success, nil, error)
                
            } else {
                completion(success, nil, error)
            }
        }
    }

    
    // MARK: Reasons
    open func GetReasons(completion: @escaping (_ success: Bool, _ object: [Reason]?, _ error:HeyJudeError?) ->()) {
        get(request: createGetRequest(path: "rating-reasons")) { (success, data, error) in
            if let reasons = data?.reasons {
                completion(success, reasons, error)
            } else {
                completion(false, [], error)
            }
        }
    }
    
    // MARK: (Task) Suggestions
    open func GetSuggestions(completion: @escaping (_ success: Bool, _ object: [Suggestion]?, _ error:HeyJudeError?) ->()) {
        get(request: createGetRequest(path: "suggestions")) { (success, data, error) in
            if let suggestions = data?.suggestions {
                completion(success, suggestions, error)
            } else {
                completion(false, [], error)
            }
        }
    }
    
    // MARK: Ideas
    open func GetIdeas(completion: @escaping (_ success: Bool, _ object: [Idea]?, _ error:HeyJudeError?) ->()) {
        get(request: createGetRequest(path: "ideas")) { (success, data, error) in
            if let ideas = data?.ideas {
                completion(success, ideas, error)
            } else {
                completion(false, [], error)
            }
        }
    }
    
    // MARK: Tasks
    // MARK: - Task Un/Read Status
    open func markTaskAsUnread(id: Int, completion: @escaping () -> ()) {
        post(request: createPostRequest(path: "tasks/" + String(id) + "/unread")) {  (success, data, error) in
            completion()
        }
    }
    
    open func markTaskAsRead(id: Int, completion: @escaping () -> ()) {
        post(request: createPostRequest(path: "tasks/" + String(id) + "/read")) {  (success, data, error) in
            completion()
        }
    }
    
    // MARK: - Open
    open func OpenTasks(completion: @escaping (_ success: Bool, _ object: [Task]?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "tasks/open")) { (success, data, error) in
            if let tasks = data?.tasks {
                completion(success, tasks, error)
            } else {
                completion(false, [], error)
            }
        }
    }
    
    // MARK: - Closed
    open func ClosedTasks(completion: @escaping (_ success: Bool, _ object: [Task]?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "tasks/closed")) { (success, data, error) in
            
            if let tasks = data?.tasks {
                completion(success, tasks, error)
            } else {
                completion(false, [], error)
            }
        }
    }
    
    // MARK: - Get Task
    open func GetTask(id: Int, completion: @escaping (_ success: Bool, _ object: Task?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "tasks/" + String(id))) { (success, data, error) in
            
            if (success) {
                completion(success, data?.task, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Cancel Task
    open func CancelTask(id: Int, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "tasks/" + String(id) + "/cancel")) { (success, data, error) in
            
            if (success) {
                completion(success, error)
            } else {
                completion(success, error)
            }
        }
    }
    
    // MARK: - Reopen Task
    open func ReopenTask(id: Int, completion: @escaping (_ success: Bool, _ object: Any?,  _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "tasks/" + String(id) + "/reopen")) { (success, data, error) in
            if success {
                if let roadblock = data?.roadblock {
                    completion(success, roadblock, error)
                } else {
                    completion(success, nil, error)
                }
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Delete Task
    open func DeleteTask(id: Int, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "tasks/" + String(id) + "/delete")) { (success, data, error) in
            if (success) {
                completion(success, error)
            } else {
                completion(success, error)
            }
        }
    }
    
    // MARK: - Setting
    open func getSettingsString(completion: @escaping (_ success: Bool, _ data: String?, _ error: HeyJudeError?) -> ()) {
        getGenericTaskString(request: createGetRequest(path: "settings"), completion: { (success, data, error) in
            completion(success, data ?? "", error)
        })
    }
    
    private func getGenericTaskString(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ data: String?, _ error: HeyJudeError?) -> ()) {
        dataTaskString(request: request, method: "GET", completion: completion)
    }
    
    // MARK: - Create Task with Idea
    open func CreateTask(title: String, createDefaultMessage: Bool, ideaId: String?, taskSuggestionId:String?, completion: @escaping (_ success: Bool, _ object: Any?, _ error: HeyJudeError?) -> ()) {
        let lat = self.currentLocation?.coordinate.latitude
        let lon = self.currentLocation?.coordinate.longitude
        var latString = ""
        var lonString = ""
        
        if lat != nil {
            latString = "\(lat ?? 0)"
        }
        if lon != nil {
            lonString = "\(lon ?? 0)"
        }
        
        let ideaIdString = ideaId ?? ""
        let taskSuggestionIdString = taskSuggestionId ?? ""
        
        let params = ["title": title, "create_default_message": createDefaultMessage, "ideaId": ideaIdString, "task_suggestion_id": taskSuggestionIdString, "latitude": latString, "longitude": lonString] as [String : Any]
        
        
        post(request: createPostRequest(path: "tasks/create", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                if let task = data?.task {
                    completion(success, task, error)
                    return
                }
                if let roadblock = data?.roadblock {
                    completion(success, roadblock, error)
                    return
                }
                completion(success, nil, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Create Task with Idea and Messages
    open func CreateTask(title: String, createDefaultMessage: Bool, ideaId: String?, taskSuggestionId:String?, messages: Any?, completion: @escaping (_ success: Bool, _ object: Any?, _ error: HeyJudeError?) -> ()) {
        let lat = self.currentLocation?.coordinate.latitude
        let lon = self.currentLocation?.coordinate.longitude
        var latString = ""
        var lonString = ""
        
        if lat != nil {
            latString = "\(lat ?? 0)"
        }
        if lon != nil {
            lonString = "\(lon ?? 0)"
        }
        
        let ideaIdString = ideaId ?? ""
        let taskSuggestionIdString = taskSuggestionId ?? ""
        
        let params = ["title": title, "create_default_message": createDefaultMessage, "ideaId": ideaIdString, "task_suggestion_id": taskSuggestionIdString, "latitude": latString, "longitude": lonString, "messages": messages as Any ] as [String : Any]
        
        post(request: createPostRequest(path: "tasks/create", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                if let task = data?.task {
                    completion(success, task, error)
                    return
                }
                if let roadblock = data?.roadblock {
                    completion(success, roadblock, error)
                    return
                }
                completion(success, nil, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    
    // MARK: - Create Task
    open func CreateTask(title: String, createDefaultMessage: Bool, completion: @escaping (_ success: Bool, _ object: Any?, _ error: HeyJudeError?) -> ()) {
        let lat = self.currentLocation?.coordinate.latitude
        let lon = self.currentLocation?.coordinate.longitude
        var latString = ""
        var lonString = ""
        
        if lat != nil {
            latString = "\(lat ?? 0)"
        }
        if lon != nil {
            lonString = "\(lon ?? 0)"
        }
        
        let params = ["title": title, "create_default_message": createDefaultMessage, "latitude": latString, "longitude": lonString] as [String : Any]
        post(request: createPostRequest(path: "tasks/create", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                if let task = data?.task {
                    completion(success, task, error)
                    return
                }
                if let roadblock = data?.roadblock {
                    completion(success, roadblock, error)
                    return
                }
                completion(success, nil, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Message Task
    open func MessageTask(id: Int, params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "tasks/" + String(id) + "/message", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                completion(success, error)
            } else {
                completion(success, error)
            }
        }
    }
    
    open func MessageTask(id: Int, params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ object: Any?, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "tasks/" + String(id) + "/message", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                if let message = data?.message {
                    completion(success, message, error)
                    return
                }
                
                if let roadblock = data?.roadblock {
                    completion(success, roadblock, error)
                    return
                }
                
                completion(success, nil, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Rate Task
    open func RateTask(id: Int, params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "tasks/" + String(id) + "/rate", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                completion(success, error)
            } else {
                completion(success, error)
            }
        }
    }
    
    // MARK: - Get Task Rating
    open func TaskRating(id: Int, unixTimeStamp: String, completion: @escaping (_ success: Bool, _ rating: Int?, _ error: HeyJudeError?) -> ()) {
        let params = ["unix_timestamp": unixTimeStamp]
        post(request: createPostRequest(path: "tasks/" + String(id) + "/rating", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                completion(success, data?.taskRating, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Task Satisfaction Probe Get
    // params needed, connotation ["positive" || "negative"]
    open func TaskStatisfactionProbes(connotation: String, completion: @escaping (_ success: Bool, _ object: [TaskSafisfactionProbe]?, _ error: HeyJudeError?) -> ()) {
        
        get(request: createGetRequest(path: "task-satisfaction-probes/" + connotation)) { (success, data, error) in
            if success {
                if let taskSafisfactionProbes = data?.taskSafisfactionProbes {
                    completion(success, taskSafisfactionProbes, error)
                } else {
                    completion(false, nil, nil)
                }
            } else {
                completion(success, nil, error)
            }
        }
        
    }
    
    // MARK: - Task Satisfaction Probe result POST
    open func TaskSatisfactionProbeResult(taskId: Int, connotation: String, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
//        let params = [
//            "task_satisfaction_probe_ids": probes
//        ] as Dictionary<String, AnyObject>
        
        let params = [
            "connotation": connotation
        ] as Dictionary<String, AnyObject>
        
        post(request: createPostRequest(path: "tasks/\(taskId)/task-satisfaction-probes", params: params)) { (success, data, error) in
            if (success) {
                completion(success, nil)
            } else {
                completion(success, error)
            }
        }
    }
    
    // MARK: - Task Support
    // Params needed, taskId, message
    open func TaskSupportRequest(taskId: Int, params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ object: AnyObject?, _ error: HeyJudeError?) -> ()) {

        post(request: createPostRequest(path: "tasks/\(taskId)/support", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                completion(success, data, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: Attachments
    // MARK: - Upload
    open func UploadAttachment(path: String, completion: @escaping (_ success: Bool, _ object: Attachment?, _ error: HeyJudeError?) -> ()) {
        post(request: createMultiPartRequest(path: "attachments/upload", filePath: path)) { (success, data, error) in
            if (success) {
                completion(success, data?.attachment, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Download
    open func DownloadAttachment(id: Int, completion: @escaping (_ success: Bool, _ object: AnyObject?, _ error: HeyJudeError?) -> ()) {
        download(request: createDownloadRequest(path: "attachments/download/" + String(id))) { (success, data, error) in
            if (success) {
                completion(success, data, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Detail
    open func AttachmentDetail(id: Int, completion: @escaping (_ success: Bool, _ object: Attachment?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "attachments/detail/" + String(id))) { (success, data, error) in
            
            if (success) {
                completion(success, data?.attachment, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Payments
    // MARK: Methods
    open func PaymentMethods(completion: @escaping (_ success: Bool, _ object: [PaymentMethod]?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "payments/methods")) { (success, data, error) in
            if (success) {
                completion(success, data?.paymentMethods, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: Payment Provider Credentials
    public func PaymentProviders(completion: @escaping (_ success: Bool, _ object: [PaymentProvider]?, _ error: HeyJudeError?)->()) {
        get(request: createGetRequest(path: "payments/providers")) { (success, data, error) in
            if success, let providers = data?.paymentProviders {
                completion(success, providers, error)
            } else {
                completion(false, nil, error)
            }
        }
    }
    // MARK: Provider Error Fallback
    private func createPaymentAddError(provider: PaymentProviderIdentifier, message: String? = nil) -> HeyJudeError {
        return HeyJudeError(
            httpResponseCode: 404,
            apiErrors: [message ?? "Payment Provider \(provider) not supported"],
            requestError: nil,
            response: nil
        )
    }
    
    // MARK: Add Method
    public func AddPaymentMethod(paymentProviderIdentifier: String?, cardNumber: String, holder: String, expiryMonth: String, expiryYear: String, cvv: String, nickname: String, isDefault: Bool, completion: @escaping (_ success: Bool, _ object: Any?, _ error: HeyJudeError?) -> ()) {
        
        self.PaymentProviders { success, providers, error in
            
            if !success, let serverProviders = providers, serverProviders.isEmpty {
                if let judeError = error {
                    completion(false, nil, judeError)
                } else {
                    completion(false, nil, self.createPaymentAddError(provider: .peach, message: "Unfortunately, we can't add your card at the moment"))
                }
                return
            }
            
            guard let providerList = providers else {
                completion(false, nil, self.createPaymentAddError(provider: .peach, message: "Unfortunately, we can't add your card at the moment"))
                return
            }
            
            guard let providerString = paymentProviderIdentifier,
                let paymentProvider = providerList.first(where: {$0.identifier == providerString || $0.provider == providerString}),
                let paymentProviderName = paymentProvider.provider,
                let providerIdentifier = PaymentProviderIdentifier.init(rawValue: paymentProviderName ) else {
                    completion(false, nil, self.createPaymentAddError(provider: .peach, message:"problem with \(paymentProviderIdentifier ?? "provider")"))
                    return
            }
            
                   
//            let bin = String(cardNumber.prefix(6))
//            var brand = "unknown"
//            let opts: String.CompareOptions = [.regularExpression, .anchored, .caseInsensitive]
//
//            if bin.range(of: "4[0-9]", options: opts) != nil { brand = "VISA" }
//            else if bin.range(of: "5[1-5]", options: opts) != nil { brand = "MASTER" }
//            else if bin.range(of: "3[147]", options: opts) != nil { brand = "AMEX" }
//            else if (222100...272099).contains(Int(bin)!) { brand = "MASTER" }

            var params: [String: String] = [:]

            //this switch builds the params for tokenization
            switch providerIdentifier {
            case .peach:
                params = [
//                    "paymentBrand": brand,
                    "card.holder": holder,
                    "card.number": cardNumber,
                    "card.expiryMonth": expiryMonth,
                    "card.expiryYear": (expiryYear.count == 2 ? "20" : "" ) + expiryYear,
                    "card.cvv": cvv,
                ]
            case .stripe:
                params = [
                    "card[number]": cardNumber,
                    "card[exp_month]": expiryMonth,
                    "card[exp_year]": expiryYear,
                    "card[cvc]": cvv,
                ]
            default:
                completion(false, nil, self.createPaymentAddError(provider: providerIdentifier))
                return
            }
                   
            // Add tokenize3DSCardRequest function
            //creates the tokenization request for specific provider
            guard let cardTokenizeRequest = self.tokenizeCardRequest(
                paymentProviderIdentifier: providerIdentifier, paymentProvider: paymentProvider, params: params)
                else {
                    completion(false, nil, self.createPaymentAddError(provider: providerIdentifier))
                    return
            }
                   
            //this switch sends it to the correct provider endpoint to get our token,
            switch providerIdentifier {
            case .peach :
                self.tokenizeCardTaskPeach(
                    request: cardTokenizeRequest,
                    method: "POST",
                    completion: {success, response, error in
                        if success {
                            //if successful, sends the details to back end
                            self.postCard(
                                cardNumber: cardNumber,
                                holder: holder,
                                nickname: nickname,
                                isDefault:isDefault,
                                provider: providerIdentifier,
                                peachResonse: response,
                                stripeResponse: nil,
                                completion: completion
                            )
                        } else {
                            completion(false, nil, error)
                        }
                    }
                )
            case .stripe:
                self.tokenizeCardTaskStripe(
                    //if successful, sends the details to back end
                    request: cardTokenizeRequest,
                    method: "POST",
                    completion: {success, response, error in
                        if success {
                            self.postCard(
                                cardNumber: cardNumber,
                                holder: holder,
                                nickname: nickname,
                                isDefault:isDefault,
                                provider: providerIdentifier,
                                peachResonse: nil,
                                stripeResponse: response,
                                completion: completion
                            )
                        } else {
                            completion(false, nil, error)
                        }
                    }
                )
            default:
                completion(false, nil, self.createPaymentAddError(provider: providerIdentifier))
                return
            }
        }
    }
    
    private func postCard(cardNumber: String, holder: String, nickname:String, isDefault: Bool, provider: PaymentProviderIdentifier, peachResonse: PeachResponse?, stripeResponse: StripeResponse?, completion: @escaping (_ success: Bool, _ object: Any?, _ error: HeyJudeError?) -> ()) {
        var params: [String: Any] = [: ]
        
        switch provider {
        case .peach:
            guard let response = peachResonse  else {
                completion(false, nil, createPaymentAddError(provider: provider, message: "Problem with Peach cast"))
                return
            }
            
            params = [
                "provider": "peach",
                "token": response.id ?? "",
                "last_four_digits": response.card?.last4Digits ?? "",
                "expiry_month": response.card?.expiryMonth ?? "",
                "expiry_year": response.card?.expiryYear ?? "",
                "card_holder": response.card?.holder ?? "",
                "bin": response.card?.bin ?? "",
                "card_nickname": nickname,
                "default": isDefault
            ] as [String : Any]
            
        case .stripe:
            guard let response = stripeResponse else {
                completion(false, nil, createPaymentAddError(provider: provider, message: "Problem with Stripe Cast"))
                return
            }
            
            params = [
                "provider": "stripe",
                "token": response.id ?? "",
                "last_four_digits": response.card?.lastFourDigits ?? "",
                "expiry_month": String(format: "%02d", response.card?.expiryMonth ?? ""),
                "expiry_year": response.card?.expiryYear ?? "",
                "bin": String(cardNumber.prefix(6)),
                "card_holder": holder,
                "card_nickname": nickname,
                "default": isDefault
            ] as [String : Any]
            
            
        default:
            completion(false, nil, createPaymentAddError(provider: provider, message: "Problem with Provider Response"))
            return
        }
        
        self.post(
            request: self.createPostRequest(path: "payments/methods",
            params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
                if (success) {
                    if let roadblock = data?.roadblock {
                        completion(success, roadblock, error)
                        return
                    }
                    completion(success, data?.paymentMethods, error)
                } else {
                    completion(success, nil, error)
                }
            }
        
    }
    
    // MARK: Update Method
    open func UpdatePaymentMethod(id: Int, params: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ object: [PaymentMethod]?, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "payments/methods/" + String(id), params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                completion(success, data?.paymentMethods, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: Delete Method
    open func DeletePaymentMethod(id: Int, completion: @escaping (_ success: Bool, _ object: [PaymentMethod]?, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "payments/methods/" + String(id) + "/delete")) { (success, data, error) in
            if (success) {
                completion(success, data?.paymentMethods, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: Request
    open func PaymentRequest(id: Int, completion: @escaping (_ success: Bool, _ object: PaymentRequest?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "payments/requests/" + String(id))) { (success, data, error) in
            if (success) {
                completion(success, data?.paymentRequest, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: Pay
    open func Pay(paymentRequestId: Int, paymentMethodId: Int, completion: @escaping (_ success: Bool, _ object: PaymentRequest?, _ error: HeyJudeError?) -> ()) {
        let params = ["payment_request_id": paymentRequestId, "payment_method_id": paymentMethodId] as [String : Any]
        post(request: createPostRequest(path: "payments/pay", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                completion(success, data?.paymentRequest, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: History
    open func PaymentHistory(completion: @escaping (_ success: Bool, _ object: [Payment]?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "payments/history")) { (success, data, error) in
            if (success) {
                completion(success, data?.payments, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: Invoice
    open func Invoice(id: Int, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        let params = ["payment_id": id] as [String : Any]
        post(request: createPostRequest(path: "payments/invoice", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                completion(success, error)
            } else {
                completion(success, error)
            }
        }
    }
    
    // MARK: Subscription
    // MARK: - Preview Options
    open func PreviewSubscriptionOptions(country: String, completion: @escaping (_ success: Bool, _ object: [SubscriptionOption]?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "subscriptions/preview-options?country=" + country)) { (success, data, error) in
            if (success) {
                completion(success, data?.subscriptionOptions, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    open func PreviewSubscriptionOptions(country: String, profileId: String, completion: @escaping (_ success: Bool, _ object: [SubscriptionOption]?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "subscriptions/preview-options?country=" + country + "&profile_id=" + profileId)) { (success, data, error) in
            if (success) {
                completion(success, data?.subscriptionOptions, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Options
    open func SubscriptionOptions(promoCode: String? = nil, completion: @escaping (_ success: Bool, _ object: [SubscriptionOption]?, _ error: HeyJudeError?) -> ()) {
        
        var path = "subscriptions/options"
        
        if(!(promoCode?.isEmpty ?? true)) {
            path += "?promo_code=\(promoCode! as String)"
        }
        
        get(request: createGetRequest(path: path)) { (success, data, error) in
            if (success) {
                completion(success, data?.subscriptionOptions, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Status
    open func SubscriptionStatus(completion: @escaping (_ success: Bool, _ object: SubscriptionStatus?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "subscriptions/status")) { (success, data, error) in
            if (success) {
                completion(success, data?.subscriptionStatus, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Activate
    open func SubscriptionActivate(completion: @escaping (_ success: Bool, _ object: SubscriptionStatus?, _ error: HeyJudeError?) -> ()) {
        post(request: createPostRequest(path: "subscriptions/activate")) { (success, data, error) in
            if (success) {
                completion(success, data?.subscriptionStatus, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Select
    open func SubscriptionSelect(id: Int, completion: @escaping (_ success: Bool, _ object: SubscriptionStatus?, _ error: HeyJudeError?) -> ()) {
        let params = ["subscription_option_id": id] as [String : Any]
        post(request: createPostRequest(path: "subscriptions/select", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                completion(success, data?.subscriptionStatus, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Auto Renew
    open func SubscriptionAutoRenew(autoRenew: Bool, completion: @escaping (_ success: Bool, _ object: SubscriptionStatus?, _ error: HeyJudeError?) -> ()) {
        let params = ["auto_renew": autoRenew] as [String : Any]
        post(request: createPostRequest(path: "subscriptions/auto-renew", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
            if (success) {
                completion(success, data?.subscriptionStatus, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Contacts
    open func GetContactCenters(completion: @escaping (_ success: Bool, _ object:[ContactCenter]?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "contact-centers")) { (success, data, error) in
            if let contactCenters = data?.contactCenters {
                completion(success, contactCenters, error)
            } else {
                completion(false, [], error)
            }
        }
    }
    
    // MARK: Analytics
    open func Analytics(pushToken: String?, completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        if(self.token.isEmpty){
            //Exception 401 thrown
            return
        }
        
        let screenSize = UIScreen.main.nativeBounds
        let screenWidth = Int(screenSize.width)
        let screenHeight = Int(screenSize.height)
        
        
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        // Get carrier name
        let carrierName = carrier?.carrierName ?? ""
        
        //self.locationManager.requestWhenInUseAuthorization()
        
        let lat = self.currentLocation?.coordinate.latitude
        let lon = self.currentLocation?.coordinate.longitude
        var latString = ""
        var lonString = ""
        
        if lat != nil {
            latString = "\(lat ?? 0)"
        }
        if lon != nil {
            lonString = "\(lon ?? 0)"
        }
        
        var locationEnabled = false
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationEnabled = true
            break
        case .authorizedAlways:
            locationEnabled = true
            break
        default:
            locationEnabled = false
            break
        }
        
        let pushToken = pushToken ?? ""
        
        DispatchQueue.main.async {
            let params = [
                "device_identifier": UIDevice.current.name,
                "device_manufacturer": "Apple",
                "device_model": identifier,
                "device_os": "iOS",
                "device_os_version": UIDevice.current.systemVersion,
                "device_carrier": carrierName,
                "device_screen_size": "\(screenWidth)x\(screenHeight)",
                "app_version": self.appVersion,//"3.0.0",
                "app_push_enabled": UIApplication.shared.isRegisteredForRemoteNotifications,
                "app_location_enabled": locationEnabled,
                "latitude": latString,
                "longitude": lonString,
                "device_token": pushToken,
                "push_provider": "fcm",
                ] as [String : Any]
            
            self.post(request: self.createPostRequest(path: "analytics", params: params as Dictionary<String, AnyObject>?)) { (success, data, error) in
                if (success) {
                    completion(success, error)
                } else {
                    completion(success, error)
                }
            }
        }
    }
    
    // MARK: - Map
    open func Map(url: String, completion: @escaping (_ success: Bool, _ object: AnyObject?, _ error: HeyJudeError?) -> ()) {
        download(request: createMapRequest(url: url)) { (success, data, error) in
            if (success) {
                completion(success, data, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Badge Count
    open func ResetBadgeCount(completion: @escaping (_ success: Bool, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "badge")) { (success, data, error) in
            completion(success, error)
        }
    }
    
    // MARK: - FeaturedImage
    open func FeaturedImage(completion: @escaping (_ success: Bool, _ object: FeaturedImage?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "featured-image")) { (success, data, error) in
            if (success) {
                completion(success, data?.featuredImage, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Countries
    open func Countries(completion: @escaping (_ success: Bool, _ object: [Country]?, _ error: HeyJudeError?) -> ()) {
        get(request: createGetRequest(path: "countries")) { (success, data, error) in
            if (success) {
                completion(success, data?.countries, error)
            } else {
                completion(success, nil, error)
            }
        }
    }
    
    // MARK: - Licences
    open func GetLicensesURL() -> URLRequest? {
       return createGetRequest(path: "licenses?device_os=iOS&program=\(self.program)") as URLRequest
    }
    
    //MARK: Convenience Methods
    private func host() -> String! {
        switch self.environment {
            case 0: return "https://agent.heyjudeapp.com/api/v1/"
            case 1: return "https://staging.heyjudeapp.com/api/v1/"
            case 2: return "http://heyjudeapp.com.tctdigital.xyz/api/v1/"
        default: return ""
        }
    }
    
    private func socketHost() -> String! {
        switch self.environment {
//            case 0: return "wss://agent.heyjudeapp.com:443/"
//            case 1: return "wss://staging.heyjudeapp.com:443/"
//            case 2: return "ws://heyjudeapp.com.tctdigital.xyz:80/"
        case 0: return "https://chat.chronos-project.co.za/"
        case 1: return "https://chat.staging.chronos-project.co.za/"
        case 2: return "https://chat.staging.chronos-project.co.za/"
        default: return ""
        }
    }
    
    private func post(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ data: Data?, _ error: HeyJudeError?) -> ()) {
        dataTask(request: request, method: "POST", completion: completion)
    }
    
    private func get(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ data: Data?, _ error: HeyJudeError?) -> ()) {
        dataTask(request: request, method: "GET", completion: completion)
    }
    
    private func download(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ data: AnyObject?, _ error: HeyJudeError?) -> ()) {
        fileTask(request: request, method: "GET", completion: completion)
    }
    
    private func tokenizeCardRequest(paymentProviderIdentifier: PaymentProviderIdentifier, paymentProvider:PaymentProvider, params: [String: String]) -> NSMutableURLRequest? {
        var bodyParameters = params
        
        guard let providerCredentials = paymentProvider.credentials else {
            return nil
        }
        
        var urlString: String
        var request: NSMutableURLRequest
        switch paymentProviderIdentifier {
        case .peach:
            
            urlString = self.environment == 0
                            ? "https://eu-prod.oppwa.com/v1/registrations"
                            : "https://eu-test.oppwa.com/v1/registrations"
            
            bodyParameters["entityId"] = providerCredentials.entityId
            bodyParameters["currency"] = "ZAR"
            bodyParameters["amount"] = "1.00"
            bodyParameters["shopperResultUrl"] = "https://localhost/success"

            guard let safeUrl = URL(string: urlString) else {
                return nil
            }
            
            request = NSMutableURLRequest(url: safeUrl)
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.addValue("Bearer " + providerCredentials.token! , forHTTPHeaderField: "Authorization")
            
            let bodyString = bodyParameters.queryParameters
            request.httpBody = bodyString.data(using: .utf8, allowLossyConversion: true)
            
            return request
            
        case .stripe:
            
            urlString = "https://api.stripe.com/v1/tokens"
            
            guard let safeUrl = URL(string: urlString), let token = providerCredentials.token else {
                return nil
             }
             
            request = NSMutableURLRequest(url: safeUrl)
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            request.addValue("Bearer " + token, forHTTPHeaderField: "Authorization")
            request.httpBody = bodyParameters.queryParameters.data(using: .utf8, allowLossyConversion: true)
            return request
            
        default:
            return nil
        }
    }
    
    //MARK: This is a function overload as the user data delete requires a different data type in the "params" parameter
    private func createPostRequest(path: String, params: [String: [AnyObject]]?) -> NSMutableURLRequest {
        if (params != nil) {
            let urlString = self.host() + path
            let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params!, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(self.apiKey, forHTTPHeaderField: "x-api-key")
            if (self.token != "") {
                request.addValue("Bearer " + self.token, forHTTPHeaderField: "Authorization")
            }
            return request
        } else {
            let urlString = self.host() + path
            let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(self.apiKey, forHTTPHeaderField: "x-api-key")
            if (self.token != "") {
                request.addValue("Bearer " + self.token, forHTTPHeaderField: "Authorization")
            }
            return request
        }
    }
    
    private func createPostRequest(path: String, params: Dictionary<String, AnyObject>? = nil) -> NSMutableURLRequest {
        
        if (params != nil) {
            let urlString = self.host() + path
            let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params!, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(self.apiKey, forHTTPHeaderField: "x-api-key")
            if (self.token != "") {
                request.addValue("Bearer " + self.token, forHTTPHeaderField: "Authorization")
            }
            return request
        } else {
            let urlString = self.host() + path
            let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(self.apiKey, forHTTPHeaderField: "x-api-key")
            if (self.token != "") {
                request.addValue("Bearer " + self.token, forHTTPHeaderField: "Authorization")
            }
            return request
        }
    }
    
    private func createGetRequest(path: String, params: Dictionary<String, AnyObject>? = nil) -> NSMutableURLRequest {
        
        if (params != nil) {
            let urlString = self.host() + path
            let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params!, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(self.apiKey, forHTTPHeaderField: "x-api-key")
            if (self.token != "") {
                request.addValue("Bearer " + self.token, forHTTPHeaderField: "Authorization")
            }
            return request
        } else {
            let urlString = self.host() + path
            let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(self.apiKey, forHTTPHeaderField: "x-api-key")
            if (self.token != "") {
                request.addValue("Bearer " + self.token, forHTTPHeaderField: "Authorization")
            }
            return request
        }
        
    }
    
    private func createDownloadRequest(path: String, params: Dictionary<String, AnyObject>? = nil) -> NSMutableURLRequest {
        
        if (params != nil) {
            let urlString = self.host() + path
            let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params!, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(self.apiKey, forHTTPHeaderField: "x-api-key")
            if (self.token != "") {
                request.addValue("Bearer " + self.token, forHTTPHeaderField: "Authorization")
            }
            return request
        } else {
            let urlString = self.host() + path
            let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(self.apiKey, forHTTPHeaderField: "x-api-key")
            if (self.token != "") {
                request.addValue("Bearer " + self.token, forHTTPHeaderField: "Authorization")
            }
            return request
        }
        
    }
    
    private func createMapRequest(url: String) -> NSMutableURLRequest {
        if url.hasPrefix("https://agent.heyjudeapp.com/") {
            var path = url.replacingOccurrences(of: "https://agent.heyjudeapp.com/map?", with: "")
            path = path.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            
            let urlString = "https://agent.heyjudeapp.com/map?" + path
            
            let request = NSMutableURLRequest(url: URL(string: urlString)!)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(self.apiKey, forHTTPHeaderField: "x-api-key")
            if (self.token != "") {
                request.addValue("Bearer " + self.token, forHTTPHeaderField: "Authorization")
            }
            return request
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: self.host())! as URL)
        return request
        
    }
    
    private func createMultiPartRequestForProfilePicture(path: String, filePath: String, params: Dictionary<String, AnyObject>? = nil) -> NSMutableURLRequest {
        
        var body = Foundation.Data()
        let boundary = self.generateBoundaryString()
        let urlString = self.host() + path
        let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.addValue(self.apiKey, forHTTPHeaderField: "x-api-key")
        if (self.token != "") {
            request.addValue("Bearer " + self.token, forHTTPHeaderField: "Authorization")
        }
        
        let url = URL(fileURLWithPath: filePath)
        let filename = url.lastPathComponent
        let data = try! Foundation.Data(contentsOf: url)
        let mimetype = mimeType(for: path)
        
        if params != nil {
            for (key, value) in params! {
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append("\(value)\r\n")
            }
        }
        
        body.append("--\(boundary)\r\n")
        body.append("Content-Disposition: form-data; name=\"profile_image\"; filename=\"\(filename)\"\r\n")
        body.append("Content-Type: \(mimetype)\r\n\r\n")
        body.append(data)
        body.append("\r\n")
        body.append("--\(boundary)--\r\n")
        request.httpBody = body
        return request
    }
    
    private func createMultiPartRequest(path: String, filePath: String, params: Dictionary<String, AnyObject>? = nil) -> NSMutableURLRequest {
        
        var body = Foundation.Data()
        let boundary = self.generateBoundaryString()
        let urlString = self.host() + path
        let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.addValue(self.apiKey, forHTTPHeaderField: "x-api-key")
        if (self.token != "") {
            request.addValue("Bearer " + self.token, forHTTPHeaderField: "Authorization")
        }
        
        let url = URL(fileURLWithPath: filePath)
        let filename = url.lastPathComponent
        let data = try! Foundation.Data(contentsOf: url)
        let mimetype = mimeType(for: path)
        
        if params != nil {
            for (key, value) in params! {
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append("\(value)\r\n")
            }
        }
        
        body.append("--\(boundary)\r\n")
        body.append("Content-Disposition: form-data; name=\"attachment\"; filename=\"\(filename)\"\r\n")
        body.append("Content-Type: \(mimetype)\r\n\r\n")
        body.append(data)
        body.append("\r\n")
        body.append("--\(boundary)--\r\n")
        request.httpBody = body
        return request
    }
    
    private func mimeType(for path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue()
        {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue()
            {
                return mimetype as String
            }
        }
        return "application/octet-stream";
        // return "image/jpeg"
        
    }
    
    private func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    private func dataTask(request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ data: Data?, _ error: HeyJudeError?) -> ()) {
        request.httpMethod = method
        
        let session = URLSession(configuration: URLSessionConfiguration.default);
        
        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            var httpResponseCode = 0
            
            if let response = response as? HTTPURLResponse {
                httpResponseCode = response.statusCode
            }
            
            if let data = data {
                
                guard let responseObj = try? JSONDecoder().decode(Response.self, from: data) else {
                    let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: nil, requestError: error, response: response)
                    completion(false, nil, error)
                    return
                }
                
                if let token = responseObj.token {
                    if token != self.token {
                        self.token = token
                    }
                    
                }
                
                guard let responseData = try? JSONDecoder().decode(ResponseData.self, from: data) else {
                    let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: responseObj.errors, requestError: error, response: response)
                    completion(responseObj.success!, nil, error)
                    return
                }
                
                if 200...299 ~= httpResponseCode {
//                    data.jsonString()
                    completion(responseObj.success!, responseData.data, nil)
                } else {
                    let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: responseObj.errors, requestError: error, response: response)
                    completion(responseObj.success!, responseData.data, error)
                }
                
            } else {
                let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: nil, requestError: error, response: response)
                completion(false, nil, error)
            }
            }.resume();
    }
    
    private func dataTaskString(request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ data: String?, _ error: HeyJudeError?) -> ()) {
        request.httpMethod = method
        
        let session = URLSession(configuration: URLSessionConfiguration.default);
        
        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            var httpResponseCode = 0
            
            if let response = response as? HTTPURLResponse {
                httpResponseCode = response.statusCode
            }
            
            if let data = data {
                
                guard let responseObj = try? JSONDecoder().decode(Response.self, from: data) else {
                    let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: nil, requestError: error, response: response)
                    completion(false, nil, error)
                    return
                }
                
                if let token = responseObj.token {
                    if token != self.token {
                        self.token = token
                    }
                    
                }
                
                var responseData = String(decoding: data, as: UTF8.self)
                
                if let range = responseData.range(of: self.token) {
                    responseData.removeSubrange(range)
                }
                
                if 200...299 ~= httpResponseCode {
                    completion(responseObj.success!, responseData, nil)
                } else {
                    let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: responseObj.errors, requestError: error, response: response)
                    completion(responseObj.success!, responseData, error)
                }
                
            } else {
                let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: nil, requestError: error, response: response)
                completion(false, nil, error)
            }
        }.resume();
    }
    
    private func tokenizeCardTaskPeach(request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ peachResponse: PeachResponse?, _ error: HeyJudeError?) -> ()) {
        request.httpMethod = method

        let session = URLSession(configuration: URLSessionConfiguration.default);

        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in

            var httpResponseCode = 0

            if let response = response as? HTTPURLResponse {
                httpResponseCode = response.statusCode
            }

            if let data = data {

                guard let peachResponse = try? JSONDecoder().decode(PeachResponse.self, from: data) else {
                    let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: nil, requestError: error, response: response)
                    completion(false, nil, error)
                    return
                }

                if 200...299 ~= httpResponseCode {
//                    data.jsonString()
                    completion(true, peachResponse, nil)
                } else {
                    let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: [(peachResponse.result?.message)!], requestError: error, response: response)
                    completion(false, nil, error)
                }

            } else {
                let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: nil, requestError: error, response: response)
                completion(false, nil, error)
            }
            }.resume();
    }
//
    private func tokenizeCardTaskStripe(request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ stripeResponse: StripeResponse?, _ error: HeyJudeError?) -> ()) {
        request.httpMethod = method

        let session = URLSession(configuration: URLSessionConfiguration.default);

        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in

            var httpResponseCode = 0

            if let response = response as? HTTPURLResponse {
                httpResponseCode = response.statusCode
            }

            if let data = data {
                guard let stripeResponse = try? JSONDecoder().decode(StripeResponse.self, from: data) else {
                    let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: nil, requestError: error, response: response)
                    completion(false, nil, error)
                    return
                }

                if 200...299 ~= httpResponseCode {
//                    data.jsonString()
                    completion(true, stripeResponse, nil)
                } else {
                    guard let stripeErrorResponse = try? JSONDecoder().decode(StripeErrorResponse.self, from: data) else {
                        let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: nil, requestError: error, response: response)
                        completion(false, nil, error)
                        return
                    }
                    guard let errorMessage = stripeErrorResponse.error?.message else { return }
                    let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: [errorMessage], requestError: error, response: response)
                    completion(false, nil, error)
                }

            } else {
                let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: nil, requestError: error, response: response)
                completion(false, nil, error)
            }
            }.resume();
    }
    
    private func fileTask(request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ data: AnyObject?, _ error: HeyJudeError?) -> ()) {
        request.httpMethod = method
        
        let session = URLSession(configuration: URLSessionConfiguration.default);
        
        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            var httpResponseCode = 0
            
            if let response = response as? HTTPURLResponse {
                httpResponseCode = response.statusCode
            }
            
            if let data = data {
                
                if 200...299 ~= httpResponseCode {
//                    data.jsonString()
                    completion(true, data as AnyObject, nil)
                } else {
                    
                    guard let responseObj = try? JSONDecoder().decode(Response.self, from: data) else {
                        let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: nil, requestError: error, response: response)
                        completion(false, nil, error)
                        return
                    }
                    
                    if let token = responseObj.token {
                        self.token = token
                    }
                    
                    let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: responseObj.errors, requestError: error, response: response)
                    
                    completion(responseObj.success!, nil, error)
                }
                
            } else {
                let error = HeyJudeError(httpResponseCode: httpResponseCode, apiErrors: nil, requestError: error, response: response)
                completion(false, nil, error)
            }
            
            }.resume();
    }
    
    private func decodeToken(jwtToken jwt: String) -> [String: Any] {
        if jwt == "" {
            return [:]
        }
        let segments = jwt.components(separatedBy: ".")
        return self.decodeJWTPart(segments[1]) ?? [:]
    }
    
    private func base64UrlDecode(_ value: String) -> Foundation.Data? {
        var base64 = value
            .replacingOccurrences(of: "-", with: "+")
            .replacingOccurrences(of: "_", with: "/")
        
        let length = Double(base64.lengthOfBytes(using: String.Encoding.utf8))
        let requiredLength = 4 * ceil(length / 4.0)
        let paddingLength = requiredLength - length
        if paddingLength > 0 {
            let padding = "".padding(toLength: Int(paddingLength), withPad: "=", startingAt: 0)
            base64 = base64 + padding
        }
        return Foundation.Data(base64Encoded: base64, options: .ignoreUnknownCharacters)
    }
    
    private func decodeJWTPart(_ value: String) -> [String: Any]? {
        guard let bodyData = self.base64UrlDecode(value),
            let json = try? JSONSerialization.jsonObject(with: bodyData, options: []), let payload = json as? [String: Any] else {
                return nil
        }
        return payload
    }
    
}

extension Foundation.Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}

extension Dictionary : URLQueryParameterStringConvertible {
    /**
     This computed property returns a query parameters string from the given NSDictionary. For
     example, if the input is @{@"day":@"Tuesday", @"month":@"January"}, the output
     string will be @"day=Tuesday&month=January".
     @return The computed parameters string.
     */
    var queryParameters: String {
        var parts: [String] = []
        for (key, value) in self {
            let part = String(format: "%@=%@",
                              String(describing: key).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!,
                              String(describing: value).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            parts.append(part as String)
        }
        return parts.joined(separator: "&")
    }
    
}

protocol URLQueryParameterStringConvertible {
    var queryParameters: String {get}
}

extension Foundation.Data {
    func jsonString()-> String {
//        let jsonString = String(data: self, encoding: .utf8)!.replacingOccurrences(of: "\\", with: "")
//        let json = jsonString.replacingOccurrences(of: "*", with: "")
//        print("JSON String: \(String(json))")
//        replace with something that should be in the JSON you are working on
//        if json.contains("token") {
//            debugPrint("JSON String: \(String(json))")
//        }
        return ""//json
    }
}
